package osmxml

import "encoding/xml"

type Bounds struct {
	XMLName xml.Name `xml:"bounds"`
	MinLat  string   `xml:"minlat,attr"`
	MinLon  string   `xml:"minlon,attr"`
	MaxLat  string   `xml:"maxlat,attr"`
	MaxLon  string   `xml:"maxlon,attr"`
}
