package osmxml

import "encoding/xml"

type Node struct {
	XMLName xml.Name `xml:"node,allowempty"`

	ID        string `xml:"id,attr"`
	User      string `xml:"user,attr,omitempty"`
	UserID    string `xml:"uid,attr,omitempty"`
	Visible   bool   `xml:"visible,attr,omitempty"`
	Version   int    `xml:"version,attr,omitempty"`
	Changeset string `xml:"changeset,attr,omitempty"`
	Timestamp string `xml:"timestamp,attr,omitempty"`

	Lat  float64 `xml:"lat,attr"`
	Lon  float64 `xml:"lon,attr"`
	Tags []Tag   `xml:"tag"`
}
