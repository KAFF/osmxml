package osmxml

import "encoding/xml"

type Way struct {
	XMLName xml.Name `xml:"way"`

	ID        string `xml:"id,attr"`
	User      string `xml:"user,attr,omitempty"`
	UserID    string `xml:"uid,attr,omitempty"`
	Visible   bool   `xml:"visible,attr,omitempty"`
	Version   int    `xml:"version,attr,omitempty"`
	Changeset string `xml:"changeset,attr,omitempty"`
	Timestamp string `xml:"timestamp,attr,omitempty"`

	Nodes []Nd  `xml:"nd"`
	Tags  []Tag `xml:"tag"`
}

type Nd struct {
	XMLName xml.Name `xml:"nd"`

	Ref string `xml:"ref,attr"`
}
