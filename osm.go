package osmxml

import "encoding/xml"

type OSM struct {
	XMLName xml.Name `xml:"osm"`

	Version   string `xml:"version,attr,omitempty"`
	Generator string `xml:"generator,attr,omitempty"`

	Bounds Bounds `xml:"bounds,omitempty"`

	Nodes     []Node     `xml:"node"`
	Ways      []Way      `xml:"way"`
	Relations []Relation `xml:"relation"`
}
