package osmxml

import "encoding/xml"

type Relation struct {
	XMLName xml.Name `xml:"relation"`

	ID        string `xml:"id,attr"`
	User      string `xml:"user,attr,omitempty"`
	UserID    string `xml:"uid,attr,omitempty"`
	Visible   bool   `xml:"visible,attr,omitempty"`
	Version   int    `xml:"version,attr,omitempty"`
	Changeset string `xml:"changeset,attr,omitempty"`
	Timestamp string `xml:"timestamp,attr,omitempty"`

	Members []Member `xml:"member"`
	Tags    []Tag    `xml:"tag"`
}

type Member struct {
	XMLName xml.Name `xml:"member"`

	Type string `xml:"type,attr"`
	Ref  string `xml:"ref,attr"`
	Role string `xml:"role,attr"`
}
